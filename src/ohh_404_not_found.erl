-module(ohh_404_not_found).

-export([
    handle/1
]).

-include("oh.hrl").

handle(X) ->
    zx:tell("~p 404 Not Found: ~p", [self(), X]),
    #rsp{status = 404,
         content = content()}.


content() ->
    unicode:characters_to_binary([
        "<!DOCTYPE html>"
        "<html>"
            "<head>"
                "<title>"
                    "404 Not Found | ophttpd ", ophttpd:version(),
                "</title>"
            "</head>"
            "<body>"
                "<h1>404 Not Found</h1>"
                "<p>"
                    "ophttpd ", ophttpd:version(),
                "</p>"
            "</body>"
        "</html>"
    ]).
