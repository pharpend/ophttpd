%%% @doc
%%% orange pill http daemon Top-level Supervisor
%%%
%%% The very top level supervisor in the system. It only has one service branch: the
%%% client handling service. In a more complex system the client handling service would
%%% only be one part of a larger system. Were this a game system, for example, the
%%% item data management service would be a peer, as would a login credential provision
%%% service, game world event handling, and so on.
%%%
%%% See: http://erlang.org/doc/design_principles/applications.html
%%% See: http://zxq9.com/archives/1311
%%% @end

-module(oh_sup).
-vsn("0.1.0").
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

%%% API

-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.
%% @end

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).



-spec init([]) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.
%% @end

init([]) ->
    RestartStrategy = #{strategy => one_for_one,
                        intensity => 1,
                        period => 60},
    ClientsSpec = #{id => oh_clients,
                    start => {oh_clients, start_link, []}},
    StaticSpec = #{id => ohh_static,
                   start => {ohh_static, start_link, []}},
    %Clients   = {oh_clients,
    %             {oh_clients, start_link, []},
    %             permanent,
    %             5000,
    %             supervisor,
    %             [oh_clients]},
    Children  = [ClientsSpec, StaticSpec],
    {ok, {RestartStrategy, Children}}.
