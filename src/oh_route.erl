-module(oh_route).

-export([
    route/1
]).

-include("oh.hrl").

-type handler() :: atom().
-type maybe_request() :: {ok, request()}
                       | {parse_error, Reason :: any(), any(), any()}
                       | timeout.

%%% API

-spec route(maybe_request()) -> handler().
%%% @doc
%%% Determine a the handler to handle a given request
%%% @end

route({parse_error, _, _, _}) -> ohh_400_bad_request;
route(timeout)                -> ohh_408_request_timeout;
route({ok, Req})              -> route_ok(Req).

route_ok(#req{uri = "/wfc-strparse"}) ->
    ohh_wfc_strparse;
route_ok(_) ->
    ohh_static.
%route_ok(R = #req{uri = "/index.html"})  -> ohh_static;
%%route_ok(#req{method = 'GET', uri = "/favicon.ico"}) -> ohh_static;
%route_ok(_)                                          -> ohh_404_not_found.
%route_ok(_)                                          -> ohh_404_not_found.
