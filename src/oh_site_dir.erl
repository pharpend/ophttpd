% this code appears correct but I don't trust it
-module(oh_site_dir).

-export([
    %extns/0,
    ls/0
]).

-record(fi,
        {contents      :: binary(),
         last_modified :: calendar:datetime(),
         content_type  :: string()}).

%-type table() :: #{string() := #fi{}}.

% extns() ->
%     Files = ls(),
%     InitAcc = sets:new(),
%     Extn =
%         fun(Filename_str) ->
%             [_, Extn_str] = string:split(Filename_str, ".", trailing),
%             Extn_str
%         end,
%     Foldel =
%         fun(#fi{http_path = HP}, Acc) ->
%             This_Extn = Extn(HP),
%             NewAcc = sets:add_element(This_Extn, Acc),
%             NewAcc
%         end,
%     Extns_set = lists:foldl(Foldel, InitAcc, Files),
%     Extns_list = sets:to_list(Extns_set),
%     Extns_list.

ls() ->
    dir(root(), []).

dir(Root, CWDParts) ->
    %zx:tell("dir(~p, ~p)", [Root, CWDParts]),
    {ok, Fs} = file:list_dir(filename:join([Root] ++ CWDParts)),
    files(Root, CWDParts, Fs, #{}).

files(_Root, _CWDParts, [], Accum) ->
    Accum;
files(Root, CWDParts, [Filename | Names], Accum) ->
    %zx:tell("files(~p, ~p, ~p, ~p)", [Root, CWDParts, X, Accum]),
    FsPath = filename:join([Root] ++ CWDParts ++ [Filename]),
    {ok, Info} = file:read_file_info(FsPath, [{time, universal}]),
    {file_info, _Size, Type, _Access, _ATime, MTime, _CTime, _Mode, _Links, _MajorDevice, _MinorDevice, _INode, _UID, _GID} = Info,
    % If it's a file add it to the list
    NewAccum =
        case Type of
            regular ->
                HttpPath = http_path(CWDParts ++ [Filename]),
                {ok, Contents} = file:read_file(FsPath),
                ContentType = content_type(HttpPath),
                FI = #fi{contents = Contents,
                         last_modified = MTime,
                         content_type = ContentType},
                NewAcc = maps:put(HttpPath, FI, Accum),
                NewAcc;
            directory ->
                NewCWDParts = CWDParts ++ [Filename],
                %zx:tell("NewCWDParts = ~p", [NewCWDParts]),
                NewShit = dir(Root, NewCWDParts),
                maps:merge(Accum, NewShit);
            _ ->
                Accum
        end,
    files(Root, CWDParts, Names, NewAccum).


http_path(Parts) ->
    http_path(Parts, []).

http_path([], Accum) ->
    unicode:characters_to_list(Accum);
% hack to skip stupid fucking bug
http_path([[] | Parts], Accum) ->
    http_path(Parts, Accum);
http_path([Part | Parts], Accum) ->
    http_path(Parts, [Accum, $/, Part]).


root() ->
    filename:join([zx:get_home(), "woh", "_site"]).


content_type(Filename_str) ->
    filetype(extn(Filename_str)).

extn(Filename_str) ->
    [_, Extn_str] = string:split(Filename_str, ".", trailing),
    Extn_str.

%filetype("css")  -> "text/css";
%filetype("html") -> "text/html";
%filetype("ico")  -> "image/vnd.microsoft.icon";
%filetype("js")   -> "text/javascript";
%filetype("jpeg") -> "image/jpeg";
%filetype("map")  -> "application/json";
%filetype("png")  -> "image/png";


filetype("aac"   ) -> "audio/aac";
filetype("abw"   ) -> "application/x-abiword";
filetype("arc"   ) -> "application/x-freearc";
filetype("avi"   ) -> "video/x-msvideo";
filetype("azw"   ) -> "application/vnd.amazon.ebook";
filetype("bin"   ) -> "application/octet-stream";
filetype("bmp"   ) -> "image/bmp";
filetype("bz"    ) -> "application/x-bzip";
filetype("bz2"   ) -> "application/x-bzip2";
filetype("cda"   ) -> "application/x-cdf";
filetype("csh"   ) -> "application/x-csh";
filetype("css"   ) -> "text/css";
filetype("csv"   ) -> "text/csv";
filetype("doc"   ) -> "application/msword";
filetype("docx"  ) -> "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
filetype("eot"   ) -> "application/vnd.ms-fontobject";
filetype("epub"  ) -> "application/epub+zip";
filetype("gz"    ) -> "application/gzip";
filetype("gif"   ) -> "image/gif";
filetype("htm"   ) -> "text/html";
filetype("html"  ) -> "text/html";
filetype("ico"   ) -> "image/vnd.microsoft.icon";
filetype("ics"   ) -> "text/calendar";
filetype("jar"   ) -> "application/java-archive";
filetype("jpeg"  ) -> "image/jpeg";
filetype("jpg"   ) -> "image/jpeg";
filetype("js"    ) -> "text/javascript";
filetype("json"  ) -> "application/json";
filetype("jsonld") -> "application/ld+json";
filetype("map"   ) -> "application/json";
filetype("mid"   ) -> "audio/midi";
filetype("midi"  ) -> "audio/midi";
filetype("mjs"   ) -> "text/javascript";
filetype("mp3"   ) -> "audio/mpeg";
filetype("mp4"   ) -> "video/mp4";
filetype("mpeg"  ) -> "video/mpeg";
filetype("mpkg"  ) -> "application/vnd.apple.installer+xml";
filetype("odp"   ) -> "application/vnd.oasis.opendocument.presentation";
filetype("ods"   ) -> "application/vnd.oasis.opendocument.spreadsheet";
filetype("odt"   ) -> "application/vnd.oasis.opendocument.text";
filetype("oga"   ) -> "audio/ogg";
filetype("ogv"   ) -> "video/ogg";
filetype("ogx"   ) -> "application/ogg";
filetype("opus"  ) -> "audio/opus";
filetype("otf"   ) -> "font/otf";
filetype("png"   ) -> "image/png";
filetype("pdf"   ) -> "application/pdf";
filetype("php"   ) -> "application/x-httpd-php";
filetype("ppt"   ) -> "application/vnd.ms-powerpoint";
filetype("pptx"  ) -> "application/vnd.openxmlformats-officedocument.presentationml.presentation";
filetype("rar"   ) -> "application/vnd.rar";
filetype("rtf"   ) -> "application/rtf";
filetype("sh"    ) -> "application/x-sh";
filetype("svg"   ) -> "image/svg+xml";
filetype("swf"   ) -> "application/x-shockwave-flash";
filetype("tar"   ) -> "application/x-tar";
filetype("tif"   ) -> "image/tiff";
filetype("tiff"  ) -> "image/tiff";
filetype("ts"    ) -> "video/mp2t";
filetype("ttf"   ) -> "font/ttf";
filetype("txt"   ) -> "text/plain";
filetype("vsd"   ) -> "application/vnd.visio";
filetype("wav"   ) -> "audio/wav";
filetype("weba"  ) -> "audio/webm";
filetype("webm"  ) -> "video/webm";
filetype("webp"  ) -> "image/webp";
filetype("woff"  ) -> "font/woff";
filetype("woff2" ) -> "font/woff2";
filetype("xhtml" ) -> "application/xhtml+xml";
filetype("xls"   ) -> "application/vnd.ms-excel";
filetype("xlsx"  ) -> "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
filetype("xml"   ) -> "application/xml";
filetype("xul"   ) -> "application/vnd.mozilla.xul+xml";
filetype("zip"   ) -> "application/zip";
filetype("3gp"   ) -> "video/3gpp";
filetype("3g2"   ) -> "video/3gpp2";
filetype("7z"    ) -> "application/x-7z-compressed";
filetype(_       ) -> "application/octet-stream".
