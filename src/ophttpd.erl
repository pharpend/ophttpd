-module(ophttpd).
-vsn("0.1.0").
-behavior(application).

-export([refresh/0, version/0]).
-export([listen/1, ignore/0]).
-export([start/0]).
-export([start/1]).
-export([start/2, stop/1]).

-include("$zx_include/zx_logger.hrl").

%%% API

refresh() ->
    ohh_static:refresh().
version() -> "0.1.0".

-spec listen(PortNum) -> Result
    when PortNum :: inet:port_num(),
         Result  :: ok | {error, {listening, inet:port_num()}}.
% @doc
% Make the server start listening on a port.
% Returns an {error, Reason} tuple if it is already listening.
% @end

listen(PortNum) ->
    ok = oh_client_man:listen(PortNum),
    ok = tell("listening on ~w", [PortNum]),
    ok.



-spec ignore() -> ok.
% @doc
% Make the server stop listening if it is, or continue to do nothing
% if it isn't.
% @end

ignore() ->
    oh_client_man:ignore().



%%%
% start/* callgraph
%
%   % commented out because start/2 automatically listens now
%   % start/1 ->
%   %     start/0
%   %     listen/1
%   start/0 ->
%       application:start/1
%   application:start/1 ->
%       start/2
%   start/2 ->
%       oh_sup:start_link/0
%%%

-spec start() -> ok.
% @doc
% Start the server in an "ignore" state.
% @end

start() ->
    ok = application:ensure_started(sasl),
    ok = application:start(ophttpd),
    io:format("Starting...").



-spec start(PortNum) -> ok
    when PortNum :: inet:port_number().
% @doc
% Start the server and begin listening immediately. Slightly more
% convenient when playing around in the shell.
% @end

start(PortNum) ->
    ok = start(),
    ok = listen(PortNum),
    ok.



-spec start(normal, term()) -> {ok, pid()}.
% @doc
% Called by OTP to kick things off. This is for the use of the
% "application" part of OTP, not to be called by user code.
% See: http://erlang.org/doc/apps/kernel/application.html
% @end

start(normal, _Args) ->
    % crashes if start fails
    Result = {ok, _pid} = oh_sup:start_link(),
    %ok = observer:start(),
    ok = listen(port()),
    Result.



-spec stop(term()) -> ok.
% @doc
% Similar to start/2 above, this is to be called by the "application"
% part of OTP, not client code. Causes a (hopefully graceful)
% shutdown of the application.
% @end

stop(_State) ->
    ok.



%%% PRIVATE

-spec port() -> integer().
% @private
% our port
% @end

port() ->
    8080.
