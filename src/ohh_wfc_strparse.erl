-module(ohh_wfc_strparse).

-export([
    handle/1
]).

-include("oh.hrl").

handle({ok, Req}) ->
    handle_ok(Req).


handle_ok(Req = #req{method = 'POST', body = Bdy}) ->
    ok = zx:tell("wfc_strparse: ~p", [Req]),
    BdyStr = unicode:characters_to_list(Bdy),
    Response_iolist =
        case wfc_strparse:ref_safe(BdyStr) of
            {ok, Result} ->
                Result;
            {error, Message} ->
                io_lib:format("error: ~p", [Message])
        end,
    Response_binary = unicode:characters_to_binary(Response_iolist),
    ok = zx:tell("wfc_strparse response: ~ts", [Response_binary]),
    #rsp{content_type = "text/plain",
         content = Response_binary}.
