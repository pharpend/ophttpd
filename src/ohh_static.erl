-module(ohh_static).

-behavior(gen_server).
-export([
    handle/1,
    refresh/0
]).
-export([
    start_link/0,
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2
]).

-type fi() :: {fi, Contents :: binary(),
                   LastModified :: calendar:datetime(),
                   ContentType :: string()}.
-record(s,
        {cache = oh_site_dir:ls() :: #{string() => fi()}}).
%-type state() :: #s{}.

-include("oh.hrl").

%%% API: handler
handle({ok, Req}) ->
    gen_server:call(?MODULE, {handle_ok, Req}).

refresh() ->
    gen_server:cast(?MODULE, refresh).

%%% API: Startup
start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).

init(none) ->
    State = #s{},
    _Ref = erlang:send_after(1000, self(), refresh),
    {ok, State}.


%%% API: gen_server callbacks
handle_call({handle_ok, Req}, _From, State) ->
    Reply = do_handle_ok(Req, State),
    {reply, Reply, State};
handle_call(Unexpected, From, _State) ->
    zx:log("~p unexpected call from ~p: ~tp", [self(), From, Unexpected]).

% refresh the contents
handle_cast(refresh, State) ->
    NewState = do_refresh(State),
    {noreply, NewState};
handle_cast(Unexpected, _State) ->
    zx:log("~p unexpected cast: ~tp", [self(), Unexpected]).

% refresh the contents
% TODO this is a hack, do something better
handle_info(refresh, State) ->
    _Ref = erlang:send_after(1000, self(), refresh),
    NewState = do_refresh(State),
    {noreply, NewState};
handle_info(Unexpected, _State) ->
    zx:log("~p unexpected cast: ~tp", [self(), Unexpected]).

%%% Internals

% factoring this out in case it needs to be more complex down the
% line
do_refresh(_State) ->
    % for now just reset the state
    NewState = #s{},
    NewState.

do_handle_ok(_Req = #req{method = 'GET', uri = "/"}, #s{cache = #{"/index.html" := {fi, Content, LastModified, ContentType}}}) ->
    #rsp{content_type = ContentType,
         last_modified = LastModified,
         content = Content};
do_handle_ok(Req = #req{method = 'GET', uri = URI}, #s{cache = Cache}) ->
    ok = zx:tell("requested URI: ~p", [URI]),
    Response =
        case maps:find(URI, Cache) of
            error ->
                ohh_404_not_found:handle(Req);
            {ok, {fi, Content, LastModified, ContentType}} ->
                #rsp{content_type = ContentType,
                     last_modified = LastModified,
                     content = Content}
        end,
    Response;
do_handle_ok(Req, _) ->
    ohh_400_bad_request:handle(Req).

%handle_ok(#req{method = 'GET',  uri = "/"})           -> hello();
%handle_ok(#req{method = 'GET',  uri = "/index.html"}) -> hello().
%%route_ok(R = #req{method = 'POST', uri = "/index.html"}) -> hello().
%
%hello() ->
%    #rsp{content = content()}.
%
%content() ->
%    [
%        "<!DOCTYPE html>"
%        "<html>"
%            "<head>"
%                "<title>"
%                    "Hello World | ophttpd ", ophttpd:version(),
%                "</title>"
%            "</head>"
%            "<body>"
%                "<h1>Hello World</h1>"
%                "<p>"
%                    "ophttpd ", ophttpd:version(),
%                "</p>"
%            "</body>"
%        "</html>"
%    ].
