-module(oh_client).
-vsn("0.1.0").

-export([start/1]).
-export([start_link/1, init/2]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).


%%% Type and Record Definitions
-record(s, {socket       = none           :: none | gen_tcp:socket(),
            parser_state = oh_parse:new() :: oh_parse:state()}).

-type state() :: #s{}.

-include("$zx_include/zx_logger.hrl").
-include("oh.hrl").

%%% api: startup

-spec start(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
% @private
% How the oh_client_man or a prior oh_client kicks things off.
% This is called in the context of oh_client_man or the prior oh_client.
% @end

start(ListenSocket) ->
    oh_client_sup:start_acceptor(ListenSocket).



-spec start_link(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
% @private
% This is called by the oh_client_sup. While start/1 is called to iniate a startup
% (essentially requesting a new worker be started by the supervisor), this is
% actually called in the context of the supervisor.
% @end

start_link(ListenSocket) ->
    proc_lib:start_link(?MODULE, init, [self(), ListenSocket]).



-spec init(Parent, ListenSocket) -> no_return()
    when Parent       :: pid(),
         ListenSocket :: gen_tcp:socket().
% @private
% This is the first code executed in the context of the new worker itself.
% This function does not have any return value, as the startup return is
% passed back to the supervisor by calling proc_lib:init_ack/2.
% We see the initial form of the typical arity-3 service loop form here in the
% call to listen/3.
% @end

init(Parent, ListenSocket) ->
    ok = tell("~p Listening.", [self()]),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, ListenSocket).



-spec listen(Parent, Debug, ListenSocket) -> no_return()
    when Parent       :: pid(),
         Debug        :: [sys:dbg_opt()],
         ListenSocket :: gen_tcp:socket().
% @private
% This function waits for a TCP connection. The owner of the socket
% is still the oh_client_man (so it can still close it on a call to
% oh_client_man:ignore/0), but the only one calling gen_tcp:accept/1
% on it is this process. Closing the socket is one way a manager
% process can gracefully unblock child workers that are blocking on a
% network accept.
%
% Once it makes a TCP connection it will call start/1 to spawn its
% successor.
% @end

listen(Parent, Debug, ListenSocket) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, Socket} ->
            {ok, _} = start(ListenSocket),
            {ok, Peer} = inet:peername(Socket),
            ok = tell("~p Connection accepted from: ~p", [self(), Peer]),
            ok = oh_client_man:enroll(),
            State = #s{socket = Socket},
            loop(Parent, Debug, State);
        {error, closed} ->
            ok = tell("~p Retiring: Listen socket closed.", [self()]),
            exit(normal)
     end.



-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% The function called by the OTP internal functions after a system
% message has been handled. If the worker process has several
% possible states this is one place resumption of a specific state
% can be specified and dispatched.
% @end

system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).



-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% Called by the OTP inner bits to allow the process to terminate
% gracefully.  Exactly when and if this is callback gets called is
% specified in the docs: See:
% http://erlang.org/doc/design_principles/spec_proc.html#msg
% @end

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).



-spec system_get_state(State) -> {ok, State}
    when State :: state().
% @private
% This function allows the runtime (or anything else) to inspect the
% running state of the worker process at any arbitrary time.
% @end

system_get_state(State) -> {ok, State}.



-spec system_replace_state(StateFun, State) -> {ok, NewState, State}
    when StateFun :: fun(),
         State    :: state(),
         NewState :: term().
% @private
% This function allows the system to update the process state
% in-place. This is most useful for state transitions between code
% types, like when performing a hot update (very cool, but sort of
% hard) or hot patching a running system (living on the edge!).
% @end

system_replace_state(StateFun, State) ->
    {ok, StateFun(State), State}.



%%% internals

-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% The service loop itself. This is the service state. The process
% blocks on receive of Erlang messages, TCP segments being received
% themselves as Erlang messages.
% @end
loop(Parent, Debug, State = #s{socket = Socket, parser_state = PS = {partial, _, _}}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    %ok = tell("parser state: ~p", [PS]),
    receive
        {tcp, Socket, Message} ->
            NewParserState = oh_parse:parse(Message, PS),
            %ok = tell("new parser state: ~p", [NewParserState]),
            NewState = State#s{parser_state = NewParserState},
            loop(Parent, Debug, NewState);
        {tcp_closed, Socket} ->
            ok = tell("~p Socket closed, retiring.~n", [self()]),
            exit(normal);
        {system, From, Request} ->
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State);
        Unexpected ->
            ok = tell("~p Unexpected message: ~tw; state: ~tw", [self(), Unexpected, State]),
            loop(Parent, Debug, State)
    after 3000 ->
        ok = tell("request timed out", [timeout]),
        ResponseBytes = response_bytes_safe(timeout),
        ok = gen_tcp:send(Socket, ResponseBytes),
        ok = gen_tcp:shutdown(Socket, read_write),
        ok
    end;
% This is the case where we're done parsing, not expecting any more
% tcp data (incoming tcp data go to Unexpected case)
loop(_Parent, _Debug, _State = #s{socket = Socket, parser_state = {done, Request}}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    ok = tell("~p request: ~p", [self(), Request]),
    ResponseBytes = response_bytes_safe(Request),
    %ok = tell("~p response bytes:~n~s", [self(), ResponseBytes]),
    ok = gen_tcp:send(Socket, ResponseBytes),
    ok = gen_tcp:shutdown(Socket, read_write),
    ok.

response_bytes_safe(Request) ->
    try response_bytes(Request) of
        {ok, Bytes} -> Bytes
    catch
        _:_ -> err_500()
    end.

err_500() ->
    Content = <<"<h1>500 Internal Server Error</h1>">>,
    Response = #rsp{content = Content, status = 500},
    oh_render:render(Response).


response_bytes(Request) ->
    Handler = oh_route:route(Request),
    Response = Handler:handle(Request),
    ResponseBytes = oh_render:render(Response),
    ok = do_log(Request, Response),
    {ok, ResponseBytes}.


do_log(Request, Response) ->
    % Log
    {ok, #req{method = Method, uri = URI}} = Request,
    #rsp{status = Status} = Response,
    ReasonPhrase = oh_render:reason_phrase(Status),
    ok = tell("~p: ~s ~s~n"
              "~p: ~p ~s",
              [self(), Method, URI,
               self(), Status, ReasonPhrase]),
    ok.
