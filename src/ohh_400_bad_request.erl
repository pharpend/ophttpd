-module(ohh_400_bad_request).

-export([
    handle/1
]).

-include("oh.hrl").

handle(X) ->
    zx:tell("~p 400 Bad Request: ~p", [self(), X]),
    #rsp{status = 400,
         content = content()}.


content() ->
    unicode:characters_to_binary([
        "<!DOCTYPE html>"
        "<html>"
            "<head>"
                "<title>"
                    "400 Bad Request | ophttpd ", ophttpd:version(),
                "</title>"
            "</head>"
            "<body>"
                "<h1>400 Bad Request</h1>"
                "<p>"
                    "ophttpd ", ophttpd:version(),
                "</p>"
            "</body>"
        "</html>"
    ]).
