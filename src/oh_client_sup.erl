-module(oh_client_sup).
-vsn("0.1.0").
-behaviour(supervisor).

-export([start_acceptor/1]).
-export([start_link/0]).
-export([init/1]).


-spec start_acceptor(ListenSocket) -> Result
    when ListenSocket :: gen_tcp:socket(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
% @private
% Spawns the first listener at the request of the oh_client_man when
% ophttpd:listen/1 is called, or the next listener at the request of
% the currently listening oh_client when a connection is made.
%
% Error conditions, supervision strategies and other important issues
% are explained in the supervisor module docs:
% http://erlang.org/doc/man/supervisor.html
% @end

start_acceptor(ListenSocket) ->
    supervisor:start_child(?MODULE, [ListenSocket]).



-spec start_link() -> {ok, pid()}.
% @private
% This supervisor's own start function.
% @end

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, none).



-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
% @private
% The OTP init/1 function.
% @end

init(none) ->
    RestartStrategy = {simple_one_for_one, 1, 60},
    Client    = {oh_client,
                 {oh_client, start_link, []},
                 temporary,
                 brutal_kill,
                 worker,
                 [oh_client]},
    {ok, {RestartStrategy, [Client]}}.
