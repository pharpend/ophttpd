-module(ohh_408_request_timeout).

-export([
    handle/1
]).

-include("oh.hrl").

handle(X) ->
    zx:tell("~p 408 Request Timeout: ~p", [self(), X]),
    #rsp{status = 408,
         content = content()}.


content() ->
    unicode:characters_to_binary([
        "<!DOCTYPE html>"
        "<html>"
            "<head>"
                "<title>"
                    "408 Request Timeout | ophttpd ", ophttpd:version(),
                "</title>"
            "</head>"
            "<body>"
                "<h1>408 Request Timeout</h1>"
                "<p>"
                    "ophttpd ", ophttpd:version(),
                "</p>"
            "</body>"
        "</html>"
    ]).
