
-record(req,
        {method   = none                 :: none | 'GET' | 'POST',
         uri      = {uri_partial, []}    :: {uri_partial, string()} | string(),
         version  = none                 :: none | http11,
         options  = {hdrs_partial, []}   :: headers(),
         body_len = none                 :: none | integer(),
         body     = none                 :: none | binary(),
         full_req = <<>>                 :: binary()}).

-type headers() :: [{string(), string()}]
                 | {hdrs_partial, HeadersRaw :: iolist()}.
-type request() :: #req{}.

-record(rsp,
        {status        = 200                        :: integer(),
         content_type  = "text/html"                :: string(),
         last_modified = calendar:universal_time()  :: calendar:datetime(),
         connection    = closed                     :: closed | keep_alive,
         content       = <<>>                       :: binary()}).

-type response() :: #rsp{}.
