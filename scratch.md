# TODONE

- DONE get my website working
- DONE wfc interface
- DONE Internal server error code wrapper
- DONE check last video to see if it should go to air
- DONE better static build system (watch mode)
- DONE watch the static files
- DONE static file last-modified do that properly
- DONE polish wfc interface

# TODO (important)

- clean up website
- deploy
- full documentation for wfc
- wfc `tt_to_sentence` and `pptt`
- fix wfc to not have the atom thing
- finish cherl
- cherl interface

# TODO (not important)

- add a dev mode flag so that it doesn't do that refresh thing in
  production
- light mode and dark mode
- separate subdomain for static content (to reduce request volume and speed up caching)
- my own templating/build system
- get tls working
- stuff should be gzipped


# Basic flow

1. Bytes from the socket are sent to incremental parser, until the
   parser is done or errors

        partial ->
            wait for more bytes
        error ->
            BadRequest handler
            an error at this point is a 400-tier error
        done ->
            send request to router

2. Router looks at request data structure and determines which
   handler to send it to.

   In all likelihood this is just a pure function.

   An error here is a 404 error

3. Handler takes request datastructure and creates response
   datastructure

4. Renderer takes response datastructure and converts it into an
   iolist

5. Send iolist back on socket

6. Close socket

Additionally have a timing system, all of this has to finish within 3
seconds or the process is killed.

