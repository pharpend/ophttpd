orange pill http daemon
=======================

# How to run

```
$ make runlocal
$ firefox localhost:8000
```

# How to build

You need zx and the Haskell stack (go find links yourself loser)

```
$ cd woh
$ make
$ cd ..
$ make runlocal
$ firefox localhost:8080
```

The static file tree is build using Hakyll. See the readme in woh/
([`woh/README.md`](./woh/README.md))
for details. It's tracked by git, so as long as you don't make any
changes you don't need to worry about this. If you want to make
permanent changes to the static files, you need to install the
Haskell stack, because the static file tree is compiled using a
static site generator written in Haskell.

To compile your changes to the static file tree, just run `make`. Go
read the makefile if you're curious how it works.

# What is it

Example HTTP server written from scratch in Erlang using the zx
platform.

Corresponds to this YouTube playlist: <https://youtube.com/playlist?list=PLeV-0tFYTT3FbfQ5V7BflrhHXtga1cU8O>

Currently looks like when running

```
Recompile: src/oh_http_parse
Starting app otpr-ophttpd-0.1.0.
Starting.
<0.139.0> Listening.
Started [ophttpd]
<0.140.0> Listening.
<0.139.0> Connection accepted from: {{0,0,0,0,0,65535,32512,1},59670}
Monitoring <0.139.0> @ #Ref<0.3593618959.1852047363.200863>
<0.139.0> result: {ok,{req,get,"/","HTTP/1.1",
                           [{"Host","localhost:8000"},
                            {"User-Agent",
                             "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"},
                            {"Accept",
                             "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"},
                            {"Accept-Language","en-US,en;q=0.5"},
                            {"Accept-Encoding","gzip, deflate"},
                            {"DNT","1"},
                            {"Connection","keep-alive"},
                            {"Upgrade-Insecure-Requests","1"},
                            {"Cache-Control","max-age=0"}],
                           <<>>,
                           <<"GET / HTTP/1.1\r\nHost: localhost:8000\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nDNT: 1\r\nConnection: keep-alive\r\nUpgrade-Insecure-Requests: 1\r\nCache-Control: max-age=0\r\n\r\n">>}}
<0.142.0> Listening.
<0.140.0> Connection accepted from: {{0,0,0,0,0,65535,32512,1},59672}
Monitoring <0.140.0> @ #Ref<0.3593618959.1852047363.200875>
<0.140.0> result: {ok,{req,get,"/favicon.ico","HTTP/1.1",
                           [{"Host","localhost:8000"},
                            {"User-Agent",
                             "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"},
                            {"Accept","image/webp,*/*"},
                            {"Accept-Language","en-US,en;q=0.5"},
                            {"Accept-Encoding","gzip, deflate"},
                            {"DNT","1"},
                            {"Connection","keep-alive"},
                            {"Referer","http://localhost:8000/"},
                            {"Cache-Control","max-age=0"}],
                           <<>>,
                           <<"GET /favicon.ico HTTP/1.1\r\nHost: localhost:8000\r\nUser-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0\r\nAccept: image/webp,*/*\r\nAccept-Language: en-US,en;q=0.5\r\nAccept-Encoding: gzip, deflate\r\nDNT: 1\r\nConnection: keep-alive\r\nReferer: http://localhost:8000/\r\nCache-Control: max-age=0\r\n\r\n">>}}
<0.144.0> Listening.
<0.142.0> Connection accepted from: {{0,0,0,0,0,65535,32512,1},59676}
Monitoring <0.142.0> @ #Ref<0.3593618959.1852047363.200882>
<0.142.0> result: {ok,{req,get,"/","HTTP/1.1",
                           [{"Host","localhost:8000"},
                            {"User-Agent","curl/7.58.0"},
                            {"Accept","*/*"}],
                           <<>>,
                           <<"GET / HTTP/1.1\r\nHost: localhost:8000\r\nUser-Agent: curl/7.58.0\r\nAccept: */*\r\n\r\n">>}}
```

startup callgraph
=================

```
% this part is *just* to start up the application and bind to a port
zx runlocal ->
    ophttpd:start/0 ->
        application:start(ophttpd) ->
            ophttpd:start/2 ->
                port() -> 8000.
                ophttpd:listen(Port) ->
                    oh_client_man:listen(Port) ->
                        % gen_server calls down to
                        % oh_client_man:do_listen/2
                        oh_client_man:do_listen/2 ->
                            % the part that listens to a port
                            {ok, ListenSocket} = gen_tcp:listen(Port, SocketOptions)
                            oh_client:start(ListenSocket) ->
                                oh_client_sup:start_acceptor(ListenSocket) ->
                                    supervisor:start_child/2 ->
                                        oh_client:start_link(ListenSocket) ->
                                            proc_lib:start_link/3 ->
                                                oh_client:init(Parent, ListenSocket) ->
                                                    ok = tell("~p Listening.", [self()]),
                                                    Debug = sys:debug_options([]),
                                                    ok = proc_lib:init_ack(Parent, {ok, self()}),
                                                    listen(Parent, Debug, ListenSocket) ->
                                                        % waits for someone to connect and returns a "Socket", which apparently
                                                        % is distinct from a "ListenSocket"
                                                        {ok, Socket} = gen_tcp:accept(ListenSocket)
                                                        % start up a new process to listen for the next request
                                                        {ok, _} = start(ListenSocket)
                                                        % this is the dude on the other end of the wire
                                                        {ok, Peer} = inet:peername(Socket)
                                                        ok = oh_client_man:enroll()
                                                        loop/3

```

process of handling http requests
=================================

```erlang
loop(Parent, Debug, State = #s{socket = Socket}) ->
    receive
        {tcp, Socket, Message} ->
            % learned something: want to factor out this part to a
            % subprocess, because nothing else can happen while we
            % are processing the message!
            ok = handle_message(Message),
            loop(Parent, Debug, State);
        {respond, Response} ->
            ok = tell("~p responding: ~p", [self(), Response]),
            ok = gen_tcp:send(Socket, Response),
            ok = gen_tcp:close(Socket),
            exit(normal);
    end.

% TODO: use monitors here, not process_flag
% probably want to spawn a handler on process creation
handle_message(M) ->
    Parent = self(),
    % shou
    _ = process_flag(trap_exit, true),
    Pid = spawn_link(fun() -> p_handle(Parent) end),
    Pid ! {do, M},
    ok.


p_handle(Parent) ->
    receive
        {do, M} ->
            Result = do_handle_message(M),
            Parent ! {respond, Result},
            p_handle(Parent);
        die ->
            exit(normal)
    end.


do_handle_message(Message) ->
    ok = tell("~p  message: ~p", [self(), Message]),
    Request  = oh_http_parse:request(Message),
    ok = tell("~p  request: ~p", [self(), Request]),
    Response = oh_http_handle:response(Request),
    ok = tell("~p response: ~p", [self(), Response]),
    Response.
```
