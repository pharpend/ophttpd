all: emake dialyzer

emake:
	zx_include="$$HOME/zomp/lib/otpr/zx/0.12.5/include" erl -make

dialyzer:
	zx_include="$$HOME/zomp/lib/otpr/zx/0.12.5/include" dialyzer --src src

runlocal:
	zxh runlocal --libs=wfc:../wfc/
