# www_orangepill_healthcare

This is the source tree for <http://orangepill.healthcare/>

## Adding content

- static pages should go in `pages/`
- writing markdown will strip the HTML and I can't figure out how to turn that
  off. So if you need to embed html (e.g. video embed), you need to make sure
  you write the page in HTML


## Setup/Building/Running

### Setup

1. Install [The Haskell Stack][hsstack]:

   ```
   $ curl -sSL https://get.haskellstack.org/ | sh
   ```

2. Download repository:

   ```
   $ git clone https://gitlab.com/DoctorAjayKumar/www_orangepill_healthcare.git
   ```

### Build

Warning: this will take a *long* time the first time you run it, even on a fast
computer. It will also take up a good chunk of your hard drive space.

The Haskell packaging system is absolute hot garbage, but stack at least works
reliably so that's what we're using

```
$ stack setup
$ stack build
```

### Run

```
$ stack run -- site watch
```

Then point your browser at <http://localhost:8000> to see the site. It will
automatically reload

Run

```
$ stack run -- site --help
```

To see available things you can do.

### Build/Deploy

This is because I will forget

```
$ git rm -r public
$ stack clean
$ stack build
$ stack run -- site rebuild
$ mv _site public
$ git add public
$ git commit
$ git push
```

## Why Hakyll?

This uses [Hakyll][hakyll] to generate because writing HTML by hand is soooo hard, and
using Haskell makes me look smarter than I really am. But also...

- Hakyll is the best static site generator around
- Writing HTML by hand is annoying, and I'd much prefer to write Markdown
- Using Haskell means we get native access to [Pandoc][pandoc]

[hakyll]:   https://jaspervdj.be/hakyll/
[hsstack]:  https://docs.haskellstack.org/en/stable/README/
[pandoc]:   https://pandoc.org/

## TODO

- Bootstrap absolutely butchers blockquotes, so someone who's good at CSS
  should fix that, given that blockquotes are an important part of Orange Pill.

- Needs a CC license notice
- Need to set up YouTube and BitChute etc channels
- PDFs of Revelations

### DONE

- Needs to link back to source repo
- Need to fill out home page
- Need to have a link to QAnal
- Need to have a link to Craig's page
