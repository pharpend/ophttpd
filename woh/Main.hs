{-# LANGUAGE OverloadedStrings #-}

module Main where

import Hakyll
import Text.Pandoc

main :: IO ()
main =
    hakyll $ do
        favicon
        assets
        staticPages_html
        templates


favicon :: Rules ()
favicon =
    match "assets/favicon.ico" $ do
        route $ gsubRoute "assets/" (const "")
        compile copyFileCompiler

assets :: Rules ()
assets = do
    images


images :: Rules ()
images =
    match "assets/images/*" $ do
        route   idRoute
        compile copyFileCompiler


posts :: Rules ()
posts =
    match "posts/*" $ do
        route (setExtension "html")
        compile $
            pandocCompiler
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls


posts_html :: Rules ()
posts_html =
    match "posts_html/*" $ do
        route $ gsubRoute "posts_html/" (const "posts/")
        compile $
            getResourceBody
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls


staticPages :: Rules ()
staticPages =
    match "pages/*" $ do
        route $
            composeRoutes (gsubRoute "pages/" (const ""))
                          (setExtension "html")
        compile $
            pandocCompilerWith def def
                >>= loadAndApplyTemplate "templates/default.html"
                                         defaultContext
                >>= relativizeUrls


staticPages_html :: Rules ()
staticPages_html =
    match "pages_html/*" $ do
        route $ gsubRoute "pages_html/" (const "")
        compile $
            getResourceBody
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls


templates :: Rules ()
templates = match "templates/*" $ compile templateCompiler


testimonials :: Rules ()
testimonials =
    match "testimonials/*" $ do
        route idRoute
        compile $
            getResourceBody
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
                >>= relativizeUrls
