---
title: Book Recommendations
---

# Book Recommendations/Reviews

Will also include links to video playlists, because Dr. Kumar doesn't know how
to read.

## General Philosophy

- *Incerto*, by Nassim Nicholas Taleb

  **Essential**.

  This is a 4-book series, containing

  1. *Fooled by Randomness*
  2. *The Black Swan*
  3. *Antifragile*
  4. *Skin in the Game*

  All four are excellent books, but if you have to pick one, go with *Skin in
  The Game*.

  Dr. Kumar arrived at the formal intuitions behind The Orange Pill philosophy
  mostly from reading Taleb.

## Mathematics

- Linear Algebra lectures from Pavel Grinfeld

  **Essential.**

  If you to dig into real mathematics, you need to have a solid grasp of Linear
  Algebra. Professor Grinfeld gives clear, engaging, and complete explanations
  of Linear Algebra.

  * [Playlist, Part 1](https://www.youtube.com/playlist?list=PLlXfTHzgMRUKXD88IdzS14F4NxAZudSmv)
  * [Playlist, Part 2](https://www.youtube.com/playlist?list=PLlXfTHzgMRULWJYthculb2QWEiZOkwTSU)
  * [Playlist, Part 3](https://www.youtube.com/playlist?list=PLlXfTHzgMRUIqYrutsFXCOmiqKUgOgGJ5)
  * [Playlist, Part 4](https://www.youtube.com/playlist?list=PLlXfTHzgMRULZfrNCrrJ7xDcTjGr633mm)


- *Elements of Information Theory* by Thomas M. Cover and Joy A. Thomas

  Essential reading for anyone who wants to dig into the mathematics behind The
  Orange Pill philosophy

