---
title: Home
---

# Recent activity


- 2021-03-11: (Kumar) [Video: How to solve a circuit using algebraic topology (intro to homology theory)](/posts/2021-03-11-intro-to-homology.html)
- 2021-03-10: (Kumar) [Video: How to install Erlang and ZX on a fresh Ubuntu 18.04 (March 2021)](/posts/2021-03-10-installing-zx.html)
- 2021-03-10: (zxq9) [Video: Geopol: Mutual capacity for violence is the root of negotiation](/posts/2021-03-10-geopol-mutual-violence.html)

  A must-watch video which is a good introduction to the Orange Pill
  philosophy.

- 2021-03-09: (Kumar) [Video: Why does long division work?](/posts/2021-03-09-long-division.html)


# What is The Orange Pill?

It's a cluster of ideas somewhat resembling a coherent body of philosophy. It
centers around careful thought regarding uncertainty and trust. It touches upon
computer science, physics, history, and mathematics.

Currently our two projects are the [Orange Pill Storage System][opss_manifesto]
(a storage platform for building distributed applications) and QAnal (a branch
of mathematics that comes with a software package).

The term "Orange Pill" was invented by Dr. Ajay Kumar PHD.

## Projects

#### Orange Pill Storage System

The most important project is the Orange Pill Storage System (opss or OPSS).
OPSS is open-source software providing a distributed data storage platform for
people who wish to write distributed applications. Read the
[manifesto][opss_manifesto] to learn more, and to see why this is necessary.

The first piece is the [Currently Online Observation Network (COON)][coon_gitlab].

- [OPSS Manifesto][opss_manifesto]
- [OPSS GitLab][opss_gitlab]
- [COON GitLab][coon_gitlab]

#### QAnal

QAnal is short for "rational analysis". It aims to replicate much of the
functionality of the field of Analysis (study of limits) without introducing
infinite processes. The utility is for doing analysis-like things **correctly**
on computers.

In particular, one goal of QAnal is to produce a number-crunching library
similar in scope to NumPy or PyTorch, but without using floats, and generally
without substituting infinite processes with finite approximations.

Another goal of QAnal is to make advanced mathematics penetrable and accessible
to people outside of the university system.

The documentation of the math is laid out sporadically in Revelations from The
Founder ("podcast length" documents going over small mathematical points), and
in video lectures from The Founder.

- [QAnal GitLab][qanal_gitlab]

- QAnal website coming soon
- Link to PDFs of revelations coming soon
- Link to video lectures coming soon

## Who are we?

* Dr. Ajay Kumar PHD is a mathematician
* zxq9 is a distributed systems engineer

## Internal links

- [Book Recommendations][book_recommendations]
- [OPSS Manifesto][opss_manifesto]

## External links

- [COON GitLab][coon_gitlab]
- [Locals board][orangepill_locals]
- [OPSS GitLab][opss_gitlab]
- [QAnal GitLab][qanal_gitlab]
- [Source for this website][www_gitlab]

##### zxq9 Links

- [Homepage][zxq9_home]
- [Bitchute][zxq9_bitchute]
- [Gab][zxq9_gab]
- [GitLab][zxq9_gitlab]
- [Minds][zxq9_minds]
- [SageBook][zxq9_sagebook]
- [Twitter][zxq9_twitter]
- [YouTube][zxq9_youtube]

##### Kumar Links

- [BitChute][kumar_bitchute]
- [Gab][kumar_gab]
- [GitLab][kumar_gitlab]
- [Minds][kumar_minds]
- [Twitter][kumar_twitter]


[book_recommendations]:     /book_recommendations.html
[opss_manifesto]:           /opss_manifesto.html

[coon_gitlab]:              https://gitlab.com/zxq9/coon
[opss_gitlab]:              https://gitlab.com/DoctorAjayKumar/opss
[qanal_gitlab]:             https://gitlab.com/DoctorAjayKumar/qanal
[www_gitlab]:               https://gitlab.com/DoctorAjayKumar/www_orangepill_healthcare

[orangepill_locals]:        https://orangepill.locals.com/

[kumar_bitchute]:           https://www.bitchute.com/doctorajaykumar/
[kumar_gab]:                https://gab.com/DoctorAjayKumar
[kumar_gitlab]:             https://gitlab.com/DoctorAjayKumar
[kumar_minds]:              https://www.minds.com/DoctorAjayKumar/
[kumar_twitter]:            https://twitter.com/DoctorAjayKumar

[zxq9_home]:                http://zxq9.com/
[zxq9_bitchute]:            https://www.bitchute.com/zxq9/
[zxq9_gab]:                 https://gab.com/zxq9
[zxq9_gitlab]:              https://gitlab.com/zxq9
[zxq9_minds]:               https://www.minds.com/zxq9/
[zxq9_sagebook]:            https://www.sagebook.com/users/zxq9
[zxq9_twitter]:             https://twitter.com/zxq9_notits
[zxq9_youtube]:         https://www.youtube.com/channel/UCMnRVG50iFEpkgbUu1mZrMA
